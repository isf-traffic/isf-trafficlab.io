#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Python-3 script that updates data.

usage: update.py [-h] [-s] [-r] (-l | -u | -b) [--localmode]

Data updater

optional arguments:
  -h, --help        show this help message and exit
  -s, --sync        syncs with current online version
  -r, --rebuild     force data rebuild
  -l, --release     release pipeline mode
  -u, --update      update pipeline mode
  -b, --build-only  build-only pipeline mode (useful for local testing)
  --localmode       local pipeline mode

"""


import argparse
import os
import codecs
import sys
import re
import http.client
import csv
import json
import collections
import jinja2
import datalib


RELEASE_NAME = "2.2.1"
"""str: Current release name"""

SITE_DOMAIN = "isf-traffic.gitlab.io"
"""str: Site root domain."""

CHARTJS_VER = "2.7.0"
"""str: Chart.js version to use."""


def get_https_request(domain, path):
    """Fetch a simple GET request over HTTPS.

    Args:
        domain (str): Root domain name.
        path (str): Request path relative to root domain.

    Returns:
        :obj:`http.client.HTTPResponse`: Request's response object.

    """
    conn = http.client.HTTPSConnection(domain)
    conn.request("GET", path, headers={
        'User-Agent': "http.client (Ubuntu; Linux x86_64) Python 3.5.2",
        'Accept':
        'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    })
    return conn.getresponse()


def log(*args, newline=True):
    """Wrapper for print to stdout.

    Args:
        *args: Print function arguments.
        newline (:obj:`bool`, optional): New-line flag.

    """
    output = " ".join(map(str, args)) + ("\n" if newline else " ")
    #  Writes output to standard output
    sys.stdout.write(output)
    #  Flushes buffer for immediate display
    sys.stdout.flush()


CURR_DIR = os.path.dirname(__file__)
"""str: Script file's directory."""

SPEEDING_CSV = "public/speeding.csv"
"""str: Path to speeding tickets data CSV file."""

SPEEDING_JSON = "public/speeding.json"
"""str: Path to speeding tickets data JSON file."""


def update_traffic_data(datasrc=None):
    """Automate traffic data update.

    Args:
        datasrc (file-like, optional): File-like data source.
            `None` would parse data from ``rawdata/ISF-speeding.html``.

    """
    #  Fetches source tweets
    if datasrc is None:
        datasrc = open(os.path.join(CURR_DIR, "rawdata/ISF-speeding.html"))
    with datasrc:
        src_tweets = datalib.parse_tweets(datasrc, datalib.ISFTrafficTweet)
    #  Loads CSV file and builds initial JSON data
    json_data = {"data": [], "years": None, "months": None, "last": None}
    last = None
    years, months = dict(), dict()  # Years and months aggregate data
    with open(SPEEDING_CSV, "a+", newline='') as fio:
        #  Goes to beginning of file to start reading
        fio.seek(0, 0)
        writer = csv.writer(fio)
        #  Skips header if it exists, otherwise appends it
        if not fio.readline():
            writer.writerow(["Date", "Count", "Source"])
            # fio.flush():
        #  Reads all lines after header (if any) and builds initial JSON data

        def update_json(data):
            """Function that wraps updating JSON data.

            Args:
                data (:obj:`tuple`): Data 3-tuple.

            Returns:
                str: Update date string.

            """
            #  Expands data for validation
            date, count, url = data
            json_data["data"].append({
                "date": date, "count": count, "src": url,
            })
            #  Adds year and month to aggregate data
            years[date[:4]], months[date[:7]] = None, None
            return date

        for row in csv.reader(fio):
            last = update_json(row)
        #  Reads tweets data and adds new data to both CSV file and JSON data
        for tweet in src_tweets:
            data = tweet.get_traffic_data()
            if data is not None and (last is None or data[0] > last):
                writer.writerow(data)
                update_json(data)
                last = data[0]
    #  Writes JSON data
    log("\tWriting %s" % SPEEDING_JSON)
    json_data["last"] = last
    json_data["years"] = sorted(k for k in years)
    json_data["months"] = sorted(k for k in months)
    with open(SPEEDING_JSON, "w") as fio:
        json.dump(json_data, fio, indent=0)


ACCIDENTS_CSV = "public/accidents.csv"
"""str: Path to speeding tickets data CSV file."""

ACCIDENTS_JSON = "public/accidents.json"
"""str: Path to speeding tickets data JSON file."""


def build_accidents_data(src, last=None, state=None):
    """Generate accidents output data.

    Args:
        src (:obj:`listreverseiterator` of :obj:`datalib.TMOAccidentsTweet`):
            Parsed tweets in reverse order.
        last (:obj:`str`, optional): Last update (YYYY-MM-DD format).
        state (iterable, optional): History state.
            Should be convertable to a :obj:`collections.deque` instance.

    Yields:
        :obj:`tuple`: Generated data.
            Generated data is a 6-tuple of the accidents data in the first
            5 items, and the history state as a :obj:`collections.deque`
            instance in the last item.

    """
    #  History tracker
    state = collections.deque(state if state is not None else ())
    data = None  # Initialises variables to please linter!
    for tweet in src:
        data = tweet.get_summary_data()
        if data is None:
            #  Checks for event-based update data
            data = tweet.get_update_data()
            if data is not None:
                #  Adds data to history
                state.appendleft(data)
            continue
        if last is None or data[0] > last:

            def pop_update():
                """Pop a valid update from update history.

                An update is valid if its date is less or equal to
                current data's date.

                """
                try:
                    val = state.pop()
                    if val[0] > data[0]:
                        state.append(val)
                        return None
                    return val
                except IndexError:
                    return None

            writeq = collections.deque()  # Output data queue
            #  `acc` is initialised to [] to please linter!
            curr, acc, count = pop_update(), [], data[1] is None
            #  Converts data-tuple to list for updating and appends history
            # data queue.
            data = list(data + (collections.deque(),))
            while curr is not None:
                #  Based on the `pop_update` implementation, `curr` can
                # only have a date value less or equal to current data's date.
                if curr[0] < data[0]:
                    if not acc or acc[0] == curr[0]:
                        #  Increments existing accumalator's count or sets it
                        # to `curr` without a URL and extended with a history
                        # data queue (reduces unnecessary brancesh to please
                        # the linter!).
                        acc = [
                            v if i in (0, 5)
                            else None if i == 4 else v + acc[i]
                            for i, v in enumerate(curr + (acc[5],))
                        ] if acc else list(curr + (collections.deque(),))
                        acc[5].append(curr)
                    else:
                        acc[5] = tuple(acc[5])
                        writeq.append(tuple(acc))
                        acc = list(curr + (collections.deque(),))
                elif count:
                    if acc:
                        acc[5] = tuple(acc[5])
                        writeq.append(tuple(acc))
                        acc = []
                    #  Aggregates counts from update history for the same day
                    # in data record.
                    data[1] = curr[1] if data[1] is None else data[1] + curr[1]
                    data[5].append(curr)
                #  Pops next update history item
                curr = pop_update()
                if curr is None and acc and acc[0] < data[0]:
                    acc[5] = tuple(acc[5])
                    writeq.append(tuple(acc))
            #  Defaults non-resolved counts to 1 (at lease 1 accident)
            data[1] = 1 if data[1] is None else data[1]
            data[5] = tuple(data[5])
            #  Appends data to output queue as a tuple
            writeq.append(tuple(data))
            for data in writeq:
                yield data + (state,)
                last = data[0]


def update_accidents_data(datasrc=None):
    """Automate accidents data update.

    Args:
        datasrc (file-like, optional): File-like data source.
            `None` would parse data from ``rawdata/TMC-accidents-YYYY.html``.

    """
    #  Loads CSV file and builds initial JSON data
    json_data = {"data": [], "years": None, "months": None, "last": None}
    last = None
    years, months = dict(), dict()  # Years and months aggregate data
    with open(ACCIDENTS_CSV, "a+") as fio:
        #  Goes to beginning of file to start reading
        fio.seek(0, 0)
        #  Skips header if it exists, otherwise appends it
        writer = csv.writer(fio)
        if not fio.readline():
            writer.writerow([
                "Date", "Count", "Deaths", "Injuries", "Source", "Details"
            ])
        #  Reads all lines after header (if any) and builds initial JSON data

        def update_json(data):
            """Function that wraps updating JSON data.

            Args:
                data (:obj:`tuple`): Data 6-tuple.

            Returns:
                str: Update date string.

            """
            def make_item(item_data):
                """Map item data to an item dictionary."""
                return {
                    "date": item_data[0], "count": item_data[1],
                    "deaths": item_data[2], "injuries": item_data[3],
                    "src": item_data[4],
                }

            json_data["data"].append(make_item(data))
            try:
                hist = tuple(
                    tuple(item.split(";"))
                    for item in data[5].split("|") if item
                )
            except AttributeError:
                #  May be an unnecessary validation
                hist = () if data[5] is None else data[5]
            if hist:
                json_data["data"][-1]["hist"] = tuple(
                    make_item(item) for item in hist
                )
            #  Adds year and month to aggregate data
            date = data[0]
            years[date[:4]], months[date[:7]] = None, None
            return date

        for data in csv.reader(fio):
            last = update_json(data)

        #  Fetches source tweets

        def sources():
            """Generator of data sources.

            Yields:
                :obj:`listreverseiterator` of :obj:`datalib.TMOAccidentsTweet`

            """
            if datasrc is None:
                datayears = ("2014", "2015", "2016", "2017")
                minyear = datayears[0] if last is None else last[:4]
                for year in datayears:
                    if year >= minyear:
                        log("\t\tLoading TMO data %s" % year)
                        datafd = open(os.path.join(
                            CURR_DIR, "rawdata/TMC-accidents-%s.html" % year
                        ))
                        with datafd:
                            yield datalib.parse_tweets(
                                datafd, datalib.TMOAccidentsTweet
                            )
            else:
                with datasrc:
                    yield datalib.parse_tweets(
                        datasrc, datalib.TMOAccidentsTweet
                    )

        histq = None  # Update history tracker
        for src_tweets in sources():
            for data in build_accidents_data(src_tweets, last, histq):
                #  Saves state for next call (on next tweets source)
                item_date, histq = data[0], data[-1]
                if last is None or item_date > last:
                    #  Updates CSV file
                    writer.writerow(
                        ['' if v is None else v for v in data[:5]]
                        + [
                            "|".join([
                                ";".join([str(v).strip("\n") for v in prev])
                                for prev in data[5]
                            ])
                        ]
                    )
                    update_json(data[:-1])
                    last = item_date
    #  Writes JSON data
    log("\tWriting %s" % ACCIDENTS_JSON)
    json_data["last"] = last
    json_data["years"] = sorted(k for k in years)
    json_data["months"] = sorted(k for k in months)
    with open(ACCIDENTS_JSON, "w") as fio:
        json.dump(json_data, fio, indent=0)


def update_data(src_csv, search, remote_csv=None, rebuild=False):
    """Automate the data update process.

    Args:
        src_csv (str): Source CSV file path.
        search (str): Twitter search query path.
        remote_csv (:obj:`str`, optional): Remote CSV file HTTP path.
        rebuild (:obj:`bool`, optional): Force source CSV data file rebuild.

    Raises:
        :obj:`ValueError`: Unknown source CSV file.

    """
    if src_csv not in (SPEEDING_CSV, ACCIDENTS_CSV):
        raise ValueError
    updater = \
        update_traffic_data if src_csv == SPEEDING_CSV \
        else update_accidents_data
    if rebuild:
        log("\tRebuilding %s" % src_csv)
        #  Removes existing data file and rebuilds it
        try:
            os.unlink(src_csv)
        except (TypeError, OSError, IOError, FileNotFoundError):
            pass
        updater()
    #  Downloads latest remote version if path available
    remote = None
    if remote_csv is not None:
        log("\tSyncing %s with latest online version" % src_csv)
        rsp = get_https_request(SITE_DOMAIN, remote_csv)
        if rsp.status == 200:
            remote = "remote." + os.path.basename(remote_csv)
            with open(remote, "w") as fio:
                fio.write(rsp.read().decode("utf8"))
    #  Keeps the latest of any locally available versions (i.e. the larger).

    def _filesize(path):
        try:
            return os.lstat(path).st_size
        except (FileNotFoundError, TypeError):
            return 0

    try:
        os.rename(max(src_csv, remote, key=_filesize), src_csv)
    except FileNotFoundError:
        pass
    #  Creates initial data on rebuild or if file doesn't already exist
    if not os.path.lexists(src_csv):
        log("\tBuilding initial data in %s" % src_csv)
        updater()
    #  Synchronises with new data from twitter search
    log("\tSyncing %s with latest twitter data" % src_csv)
    rsp = get_https_request(datalib.TWITTER, search)
    if rsp.status == 200:
        updater(rsp)
    #  Cleans up downloaded remote
    try:
        os.unlink(remote)
    except (TypeError, OSError, IOError, FileNotFoundError):
        pass


def main():
    """Main script function."""
    parser = argparse.ArgumentParser(description="Data updater")
    parser.add_argument(
        "-s", "--sync", action="store_true",
        help="syncs with current online version"
    )
    parser.add_argument(
        "-r", "--rebuild", action="store_true", help="force data rebuild"
    )
    parser.pipeline = parser.add_mutually_exclusive_group(required=True)
    parser.pipeline.add_argument(
        "-l", "--release", action="store_true", help="release pipeline mode"
    )
    parser.pipeline.add_argument(
        "-u", "--update", action="store_true", help="update pipeline mode"
    )
    parser.pipeline.add_argument(
        "-b", "--build-only", dest="build_mode", action="store_true",
        help="build-only pipeline mode (useful for local testing)"
    )
    parser.add_argument(
        "--localmode", action="store_true", help="local pipeline mode"
    )
    args = parser.parse_args()
    if not args.build_mode:
        #  Speeding tickets data
        log("Updating speeding tickets data")
        update_data(
            SPEEDING_CSV, datalib.SEARCH_QUERY_ISF,
            "/speeding.csv" if args.sync else None, args.rebuild,
        )
        #  Accidents data
        log("Updating accidents data")
        update_data(
            ACCIDENTS_CSV, datalib.SEARCH_QUERY_TMO,
            "/accidents.csv" if args.sync else None, args.rebuild,
        )
    #  Pipeline mode
    if args.update or args.release or args.build_mode:
        if not args.build_mode:
            log(
                ("Updating pages (v%s)" if args.update else "Releasing v%s")
                % RELEASE_NAME
            )
        #  Prepares data

        def _loadlast(file_path):
            last_line = None
            with open(file_path) as file_fd:
                for line in file_fd:
                    last_line = line
            return last_line.split(",")

        def _getsizestr(file_path):
            size = float(os.lstat(file_path).st_size)
            i, unit = 0, "B"    # Initialises variables to please linter!
            for i, unit in enumerate(("B", "KB", "MB")):
                if size < 1024**(i+1):
                    break
            return "%.1f%s" % (size / 1024**i, unit)

        speeding, accidents = _loadlast(SPEEDING_CSV), _loadlast(ACCIDENTS_CSV)
        env = jinja2.Environment(loader=jinja2.FileSystemLoader("templates"))

        def writepage(page, template_name=None, **data):
            """Write public page from template.

            Args:
                page (str): Output page name.
                template_name(:obj:`str`, optional): Optional template name.
                    Defaults to using template same as page name.
                **data: Template data as keyword arguments.

            """
            template = env.get_template(
                (page if template_name is None else template_name) + ".html"
            )
            with open("public/%s.html" % page, "w") as fio:
                fio.write(template.render(
                    release=RELEASE_NAME, mode=page, website=SITE_DOMAIN,
                    speed_latest=speeding[0], acc_latest=accidents[0],
                    localmode=args.localmode,
                    jsver=CHARTJS_VER, **data
                ))

        writepage(
            "index",
            nspeed=speeding[1], srcspeed=speeding[2],
            nacc=accidents[1], ndeaths=accidents[2], ninjury=accidents[3],
            srcacc=accidents[4],
        )
        writepage(
            "speeding", template_name='speeding-template',
            last=speeding[0],
            csvsize=_getsizestr(SPEEDING_CSV),
            jsonsize=_getsizestr(SPEEDING_JSON),
        )
        writepage(
            "accidents", template_name='accidents-template',
            last=accidents[0],
            csvsize=_getsizestr(ACCIDENTS_CSV),
            jsonsize=_getsizestr(ACCIDENTS_JSON),
        )


if __name__ == "__main__":
    main()
