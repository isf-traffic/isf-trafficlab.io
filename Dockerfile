###############################################################################
# Dockerfile used by isf-traffic.gitlab.io's pipelines
# Based on a Linux Alpine image with Python 3.5
###############################################################################

FROM python:3.5-alpine

#  Author
MAINTAINER "Joseph Choufani <joseph.choufani.dev@gmail.com>"


# Update the packages repository
RUN echo "Updating packages repositories..."
RUN apk update
RUN apk upgrade

# Adds requirement file
ADD requirements.txt /opt/requirements.txt
RUN echo "Added requirements.txt"


# Install dependency packages for successfully building lxml
RUN echo "Installing dependencies"
RUN apk add gcc libc-dev python-dev libxml2 libxml2-dev libxslt libxslt-dev

# Upgrade PIP and install requirements
RUN echo "Installing requirements"
RUN pip install --upgrade pip
RUN pip install -r /opt/requirements.txt
