# -*- coding: utf-8 -*-
"""Python-3 library module containing data gathering behaviour."""


import re
import datetime
import collections
import lxml.html


__all__ = ('TimelineTweet',)
__version__ = "1.0"
__author__ = "Joseph Choufani <joseph.choufani.dev@gmail.com>"


VERSION_CODE = 7
"""int: Incremental version code. Used for easier API version comparison."""

TWITTER = "twitter.com"
"""str: Twitter root."""

TWITTER_URL = "https://" + TWITTER
"""str: Twitter root URL."""

_1DAY = datetime.timedelta(days=1)
""":obj:`datetime.timedelta`: 1 day time difference object."""


class TimelineTweet(object):
    """Class that wraps timeline tweets.

    Args:
        el_tweet (:obj:`lxml.Element`): Tweet's DIV element.

    """

    def __init__(self, el_tweet):
        """Constructor."""
        self.__el = el_tweet

    @property
    def text(self):
        """str: Tweet's text content."""
        cont = self.__el.xpath(
            './div[@class="content"]/div[@class="js-tweet-text-container"]/p'
        )[0]
        return "".join(cont.itertext())

    @property
    def timestamp(self):
        """int: Tweet's timestamp."""
        time_el = self.__el.xpath(
            './/small[@class="time"]/a/span[@data-time]'
        )[0]
        return int(time_el.get("data-time"))

    @property
    def permalink(self):
        """str: Tweet's permalink."""
        return TWITTER_URL + self.get("data-permalink-path")

    @property
    def is_reply(self):
        """bool: Flag indicating if tweet is a reply tweet with mention(s)."""
        return self.get("data-mentions") is not None

    def get(self, attr, default=None):
        """Method that wraps element's dictionary-like getter."""
        return self.__el.get(attr, default)


REG_DATE_SEP = re.compile(r"[\\\/\-]")
""":obj:`RegexObject`: Date separators regular expression."""


def get_report_date(tweet, date_comp=None):
    """Determine the date of a report tweet.

    Typically the date of the day before the tweet's date.

    Args:
        tweet (:obj:`TimelineTweet`): Report tweet.
        date_comp (:obj:`str`, optional): Extracted date string.

    Returns:
        :obj:`datetime.date`: Report date object.

    """
    parsed = None
    if date_comp is not None:
        #  Parses date string
        def parse(datestr):
            """Parse a date string.

            Args:
                datestr (str): "-" separated 0-padded components date string.

            Returns:
                :obj:`datetime.date`: Parsed date object.
                    Returns `None` if parsing failed.

            """
            for fmt in ("%Y-%m-%d", "%Y-%d-%m", "%d-%m-%Y", "%m-%d-%Y"):
                #  Iterates over possible combinations and tries to parse.
                try:
                    return \
                        datetime.datetime.strptime(datestr, fmt).date()
                except ValueError:
                    pass
            return None
        parsed = parse(
            #  Normalises string to "-" separated 0-padded components
            # for parsing
            "{:0>2}-{:0>2}-{:0>2}".format(*REG_DATE_SEP.split(date_comp))
        )
    #  Resolves date errors by comparing the parsed date to a calculated date
    # (1 day before the tweet's date). If the difference is one day, the
    # earliest date is returned, otherwise the calculated date is returned.
    calcd = (datetime.datetime.fromtimestamp(tweet.timestamp) - _1DAY).date()
    if not parsed or abs(calcd - parsed) > _1DAY:
        return calcd
    return min(parsed, calcd)


SEARCH_QUERY_ISF = "/search?f=tweets&vertical=default&q=%D8%B3%D8%B1%D8%B9%D8%A9%20OR%20%D8%B1%D8%A7%D8%AF%D8%A7%D8%B1%20from%3ALebISF&src=typd"
"""str: ISF Speeding tickets data search query."""

TRAFFIC_REG_ISF = re.compile(
    r"(?:[\s\S]*?\D)?(\d{2,})[\s\S]+?"
    r"\D(\d{1,4}\s?([\\\/\-])\s?\d{1,2}\s?\3\s?\d{1,4})\D*"
)
""":obj:`RegexObject`: ISF traffic tweet data regular expression."""

WEEK_STR = "لغاية"
"""str: Week indicator string."""

MONTH_STR = "شهر"
"""str: Month indicator string."""


class ISFTrafficTweet(TimelineTweet):
    """ISF Traffic tweet class."""

    def get_traffic_data(self):
        """Read tweet contents and return traffic data.

        Returns:
            :obj:`tuple` containing the report date, number of tickets and
            source URL extracted from tweet data.

        """
        tweet_txt = self.text
        daily_stats = TRAFFIC_REG_ISF.match(tweet_txt)
        if daily_stats is None \
           or WEEK_STR in tweet_txt or MONTH_STR in tweet_txt:
            return None
        count = daily_stats.group(1)
        report_date = get_report_date(self, daily_stats.group(2))
        #  Date is normalised to YYYY-MM-DD format and count is normalised to
        # an ASCII-digits numeric string
        return report_date.strftime("%Y-%m-%d"), int(count), self.permalink


SEARCH_QUERY_TMO = "/search?f=tweets&vertical=default&q=%22%D8%A7%D9%84%D9%A2%D9%A4%20%D8%B3%D8%A7%D8%B9%D8%A9%22%20OR%20%22%D9%A2%D9%A4%20%D8%B3%D8%A7%D8%B9%D8%A9%22%20OR%20%22%D9%86%D8%AA%D9%8A%D8%AC%D8%A9%22%20-%22%D8%B9%D9%85%D9%84%22%20from%3Atmclebanon"
"""str: TMO Accidents data search query."""

TRAFFIC_REG_TMO = (
    re.compile(r"[\s\S]*?\D(\d+)\s*حو?ادث((ا|ي)ن)?.*"),
    re.compile(r"[\s\S]*?\D(\d*)\s*قتي?ل((ا|ي)ن)?.*"),
    re.compile(r"[\s\S]*?\D(\d*)\s*جر\s?ي?ح((ا|ي)ن)?.*"),
)
""":obj:`RegexObject`: TMO accidents tweet data regular expression."""

RE_TMO_SMRY = (
    re.compile(
        r"[\s\S]*?\D(\d{1,4}\s?([\\\/\-])\s?\d{1,2}\s?\2\s?\d{1,4})\D.*"
    ),
    re.compile(r"[\s\S]*?\D(?:24|٢٤)\s*ساعة.*"),
)
""":obj:`tuple` of :obj:`re.RegexObject`:
    Regular expressions detecting TMO accidents summary tweets.
"""

RE_TMO_TOK = re.compile(
    r"((?<!\d)\d{1,2}(?!\d)(?!\s?[\\\/\-])(?!\s*?ساعة)|"
    r"\#?(?:(?<!ل)حو?ادث|(\b[\-و]\s?قتيل\b)|قتي?ل|وفاة|جر\s?ي?ح)((ا|ي)ن)?)"
)
""":obj:`re.RegexObject`: Tokenisation regular expression."""

RE_TMO_CAT = (
    re.compile(r"\#?حو?ادث"),
    re.compile(r"\#?جر\s?ي?ح"),
    re.compile(r"\#?قتي?ل|وفاة"),
)
""":obj:`tuple` of :obj:`re.RegexObject`:
    Regular expressions detecting category tokens ofTMO accidents summary
    tweets.
"""

RE_PIC_URL = re.compile(r"pic\.twitter\.com\/\w+")
""":obj:`re.RegexObject`: Regular expression used to trim picture links."""

RE_AR_TNUM = (
    re.compile(r"\bو?صفر\b"), re.compile(r"\bواحد\b"),
)
""":obj:`tuple` of :obj:`re.RegexObject`:
    Regular expressions used to replace arabic textual numbers.
"""


class TMOAccidentsTweet(TimelineTweet):
    """TMO accidents tweet class."""

    def get_update_data(self):
        """Read tweet content and return event-based update data.

        Returns:
            :obj:`tuple` containing the update's date, number of accidents,
            number of deaths, number of injuries (as integers) and source URL
            extracted from tweet data.

        """
        if self.is_reply:
            #  Replies typically signal duplicate data
            return None
        tweet_txt = self.text
        #  Skips summary tweets (never a validation too many)
        for curr in RE_TMO_SMRY:
            if curr.match(tweet_txt) is not None:
                return None
        curr = None  # Cleanup for potential type change
        naccs, ndeaths, ninjury = 1, 0, 0  # Initialisation
        mapping = TMOAccidentsTweet.map_details(tweet_txt)
        if not mapping:
            return None
        for curr in mapping:
            if RE_TMO_CAT[0].search(curr[0]) and curr[1] is not None:
                naccs = curr[1]
            elif RE_TMO_CAT[1].search(curr[0]):
                ninjury += curr[1] if curr[1] is not None else 1
            elif RE_TMO_CAT[2].search(curr[0]):
                ndeaths += curr[1] if curr[1] is not None else 1
        if (ndeaths, ninjury) == (0, 0):
            return None
        #  Emulated result
        tweet_date = datetime.datetime.fromtimestamp(self.timestamp).date()
        return (
            tweet_date.strftime("%Y-%m-%d"),
            naccs, ndeaths, ninjury, self.permalink
        )

    def get_summary_data(self):
        """Read tweet contents and return required data.

        Returns:
            :obj:`tuple` containing the report date, number of investigated
            accidents, number of deaths, number of injuries and source URL
            extracted from tweet data.

        """
        if self.is_reply:
            #  Replies typically signal duplicate data
            return None
        tweet_txt = self.text
        #  Matches the corresponding summary syntax (old vs new modes) and
        # calculates the report date either from text or based on tweet.
        # Failure to calculate a report date indicates a non-summary tweet.
        try:
            curr = RE_TMO_SMRY[0].match(tweet_txt)
            report_date = get_report_date(self, curr.group(1))
        except AttributeError:
            curr = RE_TMO_SMRY[1].match(tweet_txt)
            report_date = None if curr is None else get_report_date(self)
        if report_date is None \
           or WEEK_STR in tweet_txt or MONTH_STR in tweet_txt:
            #  Also skips weekly or monthly reports that may be matched.
            return None
        curr = None  # Cleanup for potential type change
        naccs, ndeaths, ninjury = None, 0, 0  # Initialisation
        mapping = TMOAccidentsTweet.map_details(tweet_txt)
        if not mapping:
            return None
        for curr in mapping:
            if RE_TMO_CAT[0].search(curr[0]):
                naccs = curr[1]
            elif RE_TMO_CAT[1].search(curr[0]):
                ninjury = curr[1] if curr[1] is not None else 1
            elif RE_TMO_CAT[2].search(curr[0]):
                ndeaths = curr[1] if curr[1] is not None else 1
        return (
            report_date.strftime("%Y-%m-%d"), naccs, ndeaths, ninjury,
            self.permalink,
        )

    @staticmethod
    def map_details(tweet_txt):
        """Map a string into a list of data mapping tuples.

        Args:
            tweet_txt (str): Text string to parse.

        Returns:
            :obj:`list` of :obj:`tuple` containing the token string and its
            corresponding value as :obj:`int`.

        """
        numq = collections.deque()
        tokq = collections.deque()
        last, mapping = None, []

        def poptomapping(one=False):
            """Map tokens to their values into the `mapping` sequence.

            Mapping is a :obj:`tuple` containing the token string and its
            corresponding value as a numeric string.

            Args:
                one (:obj:`bool`, optional): Flag to mapping only one token.

            """
            while tokq:
                last = tokq.pop()
                #  Maps token's value to top of `numq`. If token has special
                # single or double units, top of `numq` is kept.
                mapping.append((
                    last.group(),
                    2 if last.group(3) else numq.pop() if numq else None
                ))
                if one:
                    #  Single-token mode.
                    break

        #  Removes any `pic.twitter.com` links from tweet
        tweet_txt = RE_PIC_URL.sub("", tweet_txt)
        #  Removes any date from tweet text
        try:
            tweet_txt = tweet_txt.replace(
                RE_TMO_SMRY[0].match(tweet_txt).group(1), ""
            )
        except AttributeError:
            pass
        #  Replaces arabic textual numbers
        for i, reg in enumerate(RE_AR_TNUM):
            tweet_txt = reg.sub(str(i), tweet_txt)
        for token in RE_TMO_TOK.finditer(tweet_txt):
            _1d = token.group(2)
            if _1d is not None:
                mapping.append((_1d.strip("و").strip(), 1))
                continue
            #  Stats are converted into :obj:`int` and stacked in `numq`.
            # Category tokens are kept as they are and are stacked in `tokq`.
            try:
                curr, curq = int(token.group()), numq
            except ValueError:
                curr, curq = token, tokq
            if isinstance(curr, type(last)):
                #  Ideally, if the syntax is balanced, stats and tokens are
                # in alternating order and are stacked. An imbalanced or an
                # irregular syntax is detected with a non alternation
                # (repeated type of token) signaling the start of a new
                # pattern thus the previous token is mapped.
                poptomapping(one=True)
            curq.append(curr)
            last = curr
        poptomapping()
        return mapping


def get_tweets(html_doc, tweet_type=None):
    """Generate traffic data tweets from an HTML root element.

    Args:
        htmldoc (:obj:`lxml.Element`): HTML document root element.
            tweet_type (:obj:`TimelineTweet`, optional): Tweet class type.

    Returns:
        :obj:`listreverseiterator` of :obj:`TimelineTweet`:
            Reverse iterator over timeline tweets.

    """
    tweet_type = TimelineTweet if tweet_type is None else tweet_type
    if not issubclass(tweet_type, TimelineTweet):
        raise TypeError
    return reversed([
        tweet_type(div)
        for div in html_doc.xpath('//div[@class="stream"]/ol/li/div')
    ])


def parse_tweets(file_obj, tweet_type=None):
    """Generate traffic data tweets from a file-like object.

    Args:
        file_obj (:obj:`File`): File-like object to parse.
            tweet_type (:obj:`TimelineTweet`, optional): Tweet class type.

    Returns:
        :obj:`listreverseiterator` of :obj:`TimelineTweet`:
            Reverse iterator over timeline tweets.

    Raises:
        :obj:`TypeError`: Invalid timeline tweet class.

    """
    return get_tweets(lxml.html.parse(file_obj).getroot(), tweet_type)


def extract_tweets(html_content, tweet_type=None):
    """Generate traffic data tweets from HTML.

    Args:
        html_content (str): HTML string.
            tweet_type (:obj:`TimelineTweet`, optional): Tweet class type.

    Returns:
        :obj:`listreverseiterator` of :obj:`TimelineTweet`:
            Reverse iterator over timeline tweets.

    Raises:
        :obj:`TypeError`: Invalid timeline tweet class.


    """
    return get_tweets(lxml.html.fromstring(html_content, tweet_type))
