# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

[//]: # (Maintainer comments are added using this line's format.)

##  [2.2.1+] - 2018-09-11
### Added
-   Introduced `DAILY_UPDATE` and `FORCE_RELEASE` CI/CD control variables in
    [.gitlab-ci.yml](.gitlab-ci.yml).

### Changed
-   CI/CD trigger conditions in [.gitlab-ci.yml](.gitlab-ci.yml).
-   Separate data loading process in [notify.py](notify.py).
-   New log and update tweet strings in [notify.py](notify.py).

### Fixed
-   [Notify script](notify.py) separate update error (#39).

##  [2.2.0] - 2018-09-05
### Added
-   Dashboard casualties pie chart on Chart.js script load in
    [index.html](templates/index.html).
-   Introduced `NO_NOTIFICATION` and `NO_DEPLOY` CI/CD control variables in
    [.gitlab-ci.yml](.gitlab-ci.yml).

### Changed
-   Update's log function in (update.py)[update.py].
-   Independent data updates in (update.py)[update.py].
-   Split dashboard statistics cards in [index.html](templates/index.html).

### Removed
-   Top disclaimer message in all web pages templates in favour of disclaimer
    message at bottom.

### Fixed
-   Charts dynamic auto-scale values bug (#38).

##  [2.1.1] - 2018-05-24
### Fixed
-   Update pipeline failure (#36).

##  [2.1.0] - 2018-05-04
### Added
-   Collapsible navigation section that floats on large screens.
-   Section navigation override.

### Changed
-   Inter-pages navigation model using replace behaviour.

### Fixed
-   Speeding tickets data details overflow (#35).

##  [2.0.3] - 2018-05-01
### Changed
-   Skip current year or month data point in speeding tickets average (#34).

##  [2.0.2] - 2018-02-22
### Fixed
-   Tri-stage pipeline workaround for issue #28.
-   Handling `None` source links in Dashboard index page (#8).

##  [2.0.1] - 2018-02-21
### Fixed
-   Refresh button behaviour bug #30.
-   Cumulative checkbox inline display on small screen.

##  [2.0.0] - 2018-02-21
### Added
-   Jinja2 package requirement.
-   Common master template [master.html](templates/master.html).
-   Index page template [index.html](templates/index.html).
-   Common visualisation pages master template
    [viz-master.html](templates/viz-master.html).
-   Traffic tickets page generation template
    [speeding-template.html](templates/speeding-template.html).
-   Accidents data page generation template
    [accidents-template.html](templates/accidents-template.html).
-   Cumulative mode behaviour.

### Changed
-   Module `datalib` version code incremented to 7.
-   Page generation from Jinja2 templates in [update.py](update.py).
-   CSV data files generation using Python's `csv` module.
-   Updated Docker image with Jinja2 requirement.
-   Update navigation behaviour to be more similar to native apps (#29).

### Removed
-   Static index page `public/index.html`.
-   Static traffic tickets page `public/speeding.html`.
-   Static accidents page `public/accidents.html`.

## [1.9.0] - 2017-11-14
### Added
-   Top bar in ~~[index.html](public/index.html)~~.
-   Twitter follow button in ~~[speeding](public/speeding.html)~~'s top bar.
-   Twitter follow button in ~~[accidents](public/accidents.html)~~'s top bar.

### Changed
-   Reduced number of displayed tweets to 5 in
    ~~[index.html](public/index.html)~~.

## [1.8.3] - 2017-11-05
### Added
-   Option `--allow-csv` in [notify.py](notify.py) to enabled using CSV files.

### Changed
-   Download latest JSON files if not available in [notify.py](notify.py).

### Fixed
-   Invalid Summary Tweet on Cache Error (#21).

## [1.8.2] - 2017-11-01
### Changed
-   Enabled manual pipelines from gitlab.com

### Fixed
-   Summary Notification Error (#20).

## [1.8.1] - 2017-10-31
### Added
-   Registry image `registry.gitlab.com/isf-traffic/isf-traffic.gitlab.io`'s
    Docker file.

### Changed
-   Pipelines using the new container image from container registry
    `registry.gitlab.com/isf-traffic/isf-traffic.gitlab.io`.

## [1.8.0] - 2017-10-30
### Added
-   Notification script [notify.py](notify.py).
-   Data collection in [notify.py](notify.py) from JSON or CSV files in
    `--update` mode.
-   Release info tweet and changelog reply tweet in [notify.py](notify.py)'s
    `--release` mode.
-   Summary info tweet in [notify.py](notify.py)'s `--update` mode.

### Changed
-   Module `datalib` version code incremented to 6.
-   Updated raw data files 2017-10-30.
-   Moved and updated tweeting wrapper `update.tweet` to `notify.tweet`.
-   Dual-stage CD pipeline with notification stage separate from update stage.

### Fixed
-   Missing ISF speeding tickets report bug (#16).

### Removed
-   JSON data files in favour of generating them in pipeline.

## [1.7.2] - 2017-10-30
### Fixed
-   Timeline page section error in homepage (#15).

## [1.7.1] - 2017-10-29
### Changed
-   Updated tweets content.
-   Dashboard home description and non-free javascript notice.

## [1.7.0] - 2017-10-28
### Added
-   PyPy package `tweepy` requirement.
-   Tweeting as @isftrafficdash wrapper `update.tweet`.
-   Explicit pipeline modes `--update` and `--release` in
    [update.py](update.py).
-   Data summary/release info tweeting in pipeline mode.
-   Twitter timeline for @isftrafficdash in dashboard home.

### Changed
-   CD pipeline enabled for tags and schedules [.gitlab-ci.yml](.gitlab-ci.yml).

### Removed
-   Pipeline command line argument `--pipeline`.

## [1.6.0] - 2017-10-24
### Added
-   AJAX error handling.
-   No script warning.
-   Charts and data reading hints.
-   Logo and favicon.
-   Data files sizes in download links.
-   GNU Public License GPLv3
-   FOSS message and links to project.

### Changed
-   Tranluscent top bar.
-   Overrides links behaviour.
-   Links style.
-   Centered statistics card.
-   Moved raw data files to [rawdata](rawdata) directory.
-   Updated [README.md](README.md) for public release.

## [1.5.0] - 2017-10-21
### Added
-   Static function `datalib.TMOAccidentsTweet.map_details` to parse tally
    details.
-   TMO event-based data method `datalib.TMOAccidentsTweet.get_update_data`.
-   Separate TMO data builder function `update.build_accidents_data` with
    inferred data history.
-   Data list display toggle for inference details in TMO Accidents data page.

### Changed
-   Data methods return numbers as `int`.
-   TMO data update method with inferring from event-based updates.
-   Updated 2017 TMO data source file.
-   Module `datalib` version code incremented to 5.
-   New CSV and JSON formats including data inference details.

### Removed
-   Numeric string function `update.ints`.

## [1.4.0] - 2017-10-20
### Added
-   Reply type property in `datalib.TimelineTweet`.
-   Date parser function `datalib.get_report_date`.
-   New TMO accidents search query.
-   New TMO raw twitter data files.
-   New TMO summary data method `datalib.TMOAccidentsTweet.get_summary_data`.

### Changed
-   Updated TMO data update handler.
-   File-size-based synced remote file comparison instead of line count since
    data is uniform back again.
-   Changed update automation handler's signature. Function no longer takes
    an updater callavle and source CSV values are restricted to `SPEEDING_CSV`
    and `ACCIDENTS_CSV`.
-   Module `datalib` version code incremented to 4.

### Removed
-   TMO data method `datalib.TMOAccidentsTweet.get_accidents_data`.

## [1.3.1] - 2017-10-19
### Changed
-   Replaces static release number in HTML pages with dynamic placeholder to
    be updated in pipeline.

## [1.3.0] - 2017-10-19
### Added
-   New update behaviour `update.update_data`.
-   New combined Traffic Data updater `update.update_traffic_data`.
-   New combined Accidents Data updater `update.update_accidents_data`.

### Changed
-   Script `update.py` command-line arguments signature.
-   Update mechanism in `update.py`.
-   Module `datalib` version code incremented to 3.
-   Added sync argument to update script call in CI update pipeline.
-   Added pages release version update in pipeline mode.

### Removed
-   Traffic-data wrapper functions from `datalib`.
-   Accident-data wrapper functions from `datalib`.

## [1.2.2] - 2017-10-17
### Added
-   Non-ASCII digits handling.

### Changed
-   Module `datalib` version code incremented to 2.

## [1.2.1] - 2017-10-16
### Changed
-   Update script updates latest update date in all index pages.

## [1.2.0] - 2017-10-16
-   Accidents and casualtites data page adapted from speeding tickets page.

### Fixed
-   Dashboard stats links to categories.

## [1.1.0] - 2017-10-16
### Added
-   Speeding tickets data page adapted from old isf-tickets project.

### Changed
-   Update script updates latest update date in speeding tickets page.

## [1.0.0] - 2017-10-16
### Added
-   Dashboard home page template.
-   Library module `datalib`.
-   Base `datalib.TimelineTweet` class and `datalib.ISFTrafficTweet`, and
    `datalib.TMOAccidentsTweet` wrapper classes.
-   Data parsing wrapper functions.
-   Data update script `update.py`.
-   Data files.
-   GitLab pages CI pipeline.
