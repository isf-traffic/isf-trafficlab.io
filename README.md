# Lebanon ISF Traffic Data Dashboard

[![Build status](https://gitlab.com/isf-traffic/isf-traffic.gitlab.io/badges/master/build.svg)](https://gitlab.com/isf-traffic/isf-traffic.gitlab.io/pipelines)

Repository of aggregator site for [@LebISF](https://twitter.com/LebISF)
and [@TMCLebanon](https://twitter.com/tmclebanon) tweeted traffic stats.

##  Structure

### Data tools
-   Data tools library [datalib.py](datalib.py): provides API for parsing
    tweets data.
-   Update script [update.py](update.py): Script that automates the update
    process.
-   Raw data files in [rawdata/](rawdata): Saved full HTML pages of twitter
    searches for reports tweets.

### Aggregator site
-   Gitlab CI/CD file [.gitlab-ci.yml](.gitlab-ci.yml): CD file that updates
    data and fills template variables into site pages (via
    [update.py](update.py)'s `--update` or `--release` pipeline modes) and
    deploys the pages.
    Also used for the scheduled daily data update pipeline.
-   Notification wrapper script [notify.py](notify.py): Notification-stage
    script that tweets summary data or information release.
-   Site generation templates in [templates/](templates):
    -   Master template file [master.html](templates/master.html)
    -   Template file [index.html](templates/index.html) for lightweight
        dashboard-style page that displays the latest statistics summary and
        provides links to detailed data views.
    -   Template file [viz-master.html](templates/viz-master.html) used as base
        template for visualisation pages.
    -   Template file [speeding-template.html](templates/speeding-template.html)
        for ISF speeding tickets data page that visualises gathered ISF speeding
        tickets data in a chart and a detailed data list view.
    -   Template file [accidents-template.html](templates/accidents-template.html)
        for TMO accidents data page that visualises gathered TMO accidents data
        in a chart and a detailed data list view.
-   Default CSV data files [accidents.csv](public/accidents.csv) and
    [speeding.csv](public/speeding.csv):
    Downloadable reference data files in CSV format. Also used as
    reference for the data update process (compared to built/existing
    data) to keep the latest version of the data.

##  License

All code is released under a [GNU General Public Licence (v3)](LICENSE).

##  Feedback and Contribution

For all bugs and suggestions, a
[new issue](https://gitlab.com/isf-traffic/isf-traffic.gitlab.io/issues/new)
must be created and properly labelled with a `bug`, `suggestion`, or
`enhancement` label.

Issue title must be as succint as possible.
Issue should ideally have a description which shouldn't be redundant and
provides good information on the bug/suggestion.

Other channels of communication are also available:
-   [Twitter](https://twitter.com/isftrafficdash)
-   [Email](mailto:isftraffic.gitlab.io@gmail.com)


For code contribution, the project can be either cloned or forked. Please make
sure your merge request is associated with an issue.
