#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Python3 script for release/update notifications.

usage: notify.py [-h] (-l | -u) [-c]

Pipeline Notifier

optional arguments:
  -h, --help       show this help message and exit
  -l, --release    release mode
  -u, --update     update mode
  -c, --allow-csv  allow falling back to CSV data files

"""

import argparse
import os
import json
import sys
import subprocess
import traceback
import tweepy
from update import (
    RELEASE_NAME, SITE_DOMAIN,
    ACCIDENTS_CSV, ACCIDENTS_JSON, SPEEDING_CSV, SPEEDING_JSON,
    log, get_https_request,
)


def tweet(tweet_txt, reply_to=None):
    """Tweet as @isftrafficdata.

    Expects consumer key, consumer secret, access token and token secret in
    secret environment variables: `TWITTER_CONSUMER_KEY`,
    `TWITTER_CONSUMER_SEC`, `TWITTER_ACCESS_TOKEN`, `TWITTER_TOKEN_SECRET`.

    Args:
        tweet_txt (str): Update to post on twitter.
        reply_to (:obj:`tweepy.models.Status`, optional): Parent tweet.

    Returns:
        :obj:`tweepy.models.Status`: Posted tweet's wrapper.

    """
    #  Gathers environment variables
    consumer_key = os.environ.get("TWITTER_CONSUMER_KEY")
    consumer_secret = os.environ.get("TWITTER_CONSUMER_SEC")
    access_token = os.environ.get("TWITTER_ACCESS_TOKEN")
    access_token_secret = os.environ.get("TWITTER_TOKEN_SECRET")
    #  Authenticates app using consumer key/secret and access token/secret
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.secure = True
    auth.set_access_token(access_token, access_token_secret)
    #  Sends tweet
    opts = {"status": tweet_txt}
    if reply_to is not None:
        opts.update({"in_reply_to_status_id": reply_to.id})
    return tweepy.API(auth).update_status(**opts)


CHANGELOG_FMT = "https://gitlab.com/isf-traffic/isf-traffic.gitlab.io/" \
                "blob/isf-traffic-%s/CHANGELOG.md"
"""str: Changelog URL."""


def main():
    """Main script function."""
    parser = argparse.ArgumentParser(description="Pipeline Notifier")
    parser.pipeline = parser.add_mutually_exclusive_group(required=True)
    parser.pipeline.add_argument(
        "-l", "--release", action="store_true", help="release mode"
    )
    parser.pipeline.add_argument(
        "-u", "--update", action="store_true", help="update mode"
    )
    parser.add_argument(
        "-c", "--allow-csv", dest="csv", action="store_true",
        help="allow falling back to CSV data files"
    )
    args = parser.parse_args()

    def mutedtweet(tweet_txt, reply_to=None):
        """Error-muted tweet function."""
        try:
            return tweet(tweet_txt, reply_to)
        except tweepy.TweepError:
            log("[WARN] Failed to send tweet!")
            traceback.print_exc()
            return None

    def download(file_path):
        """Basic download wrapper."""
        rsp = get_https_request(
            SITE_DOMAIN, "/" + os.path.basename(file_path)
        )
        if rsp.status == 200:
            with open(file_path, "w") as fio:
                fio.write(rsp.read().decode("utf8"))

    if args.update:

        def get_data(file_path, run=1):
            """Load data from JSON file.
            Args:
                file_path (str): JSON file path
                run (:obj:`int`, optional): Run controller.

            Returns:
                2-tuple containing last date string and latest data mapping.

            """
            if run == 2:
                return (None,) * 2
            try:
                #  Loads data from existing JSON file (build artifact)
                with open(file_path) as json_fd:
                    tmp = json.load(json_fd)
                    return tmp["last"], tmp["data"][-1]
            except (FileNotFoundError, PermissionError, ValueError):
                #  Downloads last deployed JSON file
                log("Downloading latest %s" % file_path)
                download(file_path)
                #  Loads data recursively
                return get_data(file_path, run=run+1)  # Increments control

        #  Collects speeding update data
        data = dict()
        latest_speed, data_speed = get_data(SPEEDING_JSON)
        if latest_speed is not None and data_speed is not None:
            #  Updates data
            data.update(last_speed=latest_speed, nspeed=data_speed['count'])
        elif args.csv:
            #  Loads from CSV file's last line
            tmp = str(subprocess.run(
                ["tail", "-n1", SPEEDING_CSV],
                check=True, stdout=subprocess.PIPE
            ).stdout).strip("b'").split(",")
            data.update(last_speed=tmp[0], nspeed=tmp[1])
        #  Collects accidents update data
        latest_acc, data_acc = get_data(ACCIDENTS_JSON)
        if latest_acc is not None and data_acc is not None:
            #  Updates data
            data.update(
                last_acc=latest_acc, nacc=data_acc['count'],
                ndeaths=data_acc['deaths'], ninjury=data_acc['injuries']
            )
        elif args.csv:
            #  Loads from CSV file's last line
            tmp = str(subprocess.run(
                ["tail", "-n1", ACCIDENTS_CSV],
                check=True, stdout=subprocess.PIPE
            ).stdout).strip("b'").split(",")
            data.update(
                last_acc=tmp[0], nacc=tmp[1], ndeaths=tmp[2], ninjury=tmp[3]
            )
        if not data:
            log("[ERROR] Failed to find latest data")
            sys.exit(2)
        log(
            "Tweeting speeding data updates for {0[last_speed]} "
            "and accidents data updates for {0[last_acc]} "
            "as @isftrafficdash"
            .format(data)
        )
        stat = mutedtweet(
            "Latest statistics:\n"
            "- {nspeed} tickets for {last_speed}\n"
            "- {nacc} investigated accidents for {last_acc}"
            " ({ndeaths} deaths, {ninjury} injuries)\n"
            "https://{website}/"
            .format(website=SITE_DOMAIN, **data)
        )
    else:
        log("Tweeting release %s info as @isftrafficdash" % RELEASE_NAME)
        stat = mutedtweet(
            "Updated to new release v{release}\n"
            "https://{website}/"
            .format(release=RELEASE_NAME, website=SITE_DOMAIN)
        )
        if stat is not None:
            mutedtweet(
                "Changelog for v{release}: {chlog}".format(
                    release=RELEASE_NAME, chlog=CHANGELOG_FMT % RELEASE_NAME
                ),
                reply_to=stat
            )
    if stat is None:
        #  Exit with error on failed notification
        sys.exit(3)

if __name__ == "__main__":
    main()
